let num = 2;
let getCube = num ** 3;
 
let cubeOf = `The cube of ${num} is ${getCube}`;
console.log(cubeOf);

let address = [ 326, "Bulihan", "Malvar", "Batangas", "Philippines"];
const [houseNumber, village, municipality, city, country] = address
console.log(`I live at ${houseNumber} ${village}, ${municipality}, ${city}, ${country}`);

let animal = {
	name: "Lolong",
	weight: 1075,
	ft: 20,
	inch: 3
}
const {name, weight, ft, inch} = animal;
console.log(`${name} was a saltwater crocodile. He weighed at 
${weight} kgs with a measurement of ${ft} ft ${inch} in.`);

let numbers = [1, 2, 3, 4, 5];
numbers.forEach(eachNumber => {
    console.log(eachNumber);
});

class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed
	}
}
mypet = new Dog("Levi", "6 Months", "Toy Poodle");
console.log(mypet);

